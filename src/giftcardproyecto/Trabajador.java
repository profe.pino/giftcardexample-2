package giftcardproyecto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jpino
 */
public class Trabajador {
    private String nombre;
    private String run;
    private char dv;
    
    public Trabajador() {
        
    }
    public Trabajador(String nombre, String run, char dv ) {
        this.nombre=nombre;
        this.run= run;
        this.dv = dv;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }
    public void ver() {
        System.out.println("-------------------------------------");
        System.out.println("            TRABAJADOR");
        System.out.println("-------------------------------------");
        System.out.println(" Nombre : "+this.nombre);
        System.out.println(" Run    : "+this.run+"-"+this.dv);
        System.out.println("-------------------------------------");
    }
    
    
}
