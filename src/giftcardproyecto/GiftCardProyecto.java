/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package giftcardproyecto;
import java.util.Scanner;

/**
 *
 * @author jpino
 */
public class GiftCardProyecto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Crear Trabajador
        // Crear Tarjeta
        // Comprar
        // Consultar Tarjeta
        //Salir
        Scanner teclado = new Scanner(System.in);
        String nombre, run;
        char dv;
        Trabajador trabajador=null;
        int opcion;
        Tarjeta tarjeta = null;
        do {
           
            System.out.println("----------------------------");
            System.out.println("      MENU PRINCIPAL");
            System.out.println("----------------------------");
            System.out.println(" 1. Crear Trabajador");
            System.out.println(" 2. Consultar Trabajador");
            System.out.println(" 3. Crear Tarjeta");
            System.out.println(" 4. Comprar");
            System.out.println(" 5. Consultar Tarjeta");
            System.out.println(" 6. Salir");
            System.out.println("----------------------------");
            System.out.print(" Ingrese opcion: ");
            opcion = teclado.nextInt();
            if (opcion < 1 || opcion >6)
                System.out.println("Debe ingresar una opcion valida entre [1-5]");
            else
                switch (opcion) {
                    case 1:
                           char d_v;
                           String dv_validos = "0123456789kK";
                           System.out.print("Nombre: ");
                           nombre = teclado.next();
                           System.out.print("RUN   : ");
                           run = teclado.next();
                         /*  if (validarRut(run))
                               System.out.println("Rut es valido");
                           else
                               System.out.println("Rut no es valido");
                           */
                           trabajador = new Trabajador();
                           trabajador.setNombre(nombre);
                           trabajador.setRun(run.substring(0,10));
                           d_v = run.charAt(11);
                           if (dv_validos.contains(""+d_v+""))
                               trabajador.setDv(d_v);
                           else
                               System.out.println("DV no corresponde");
                           
                           trabajador.setDv(run.charAt(11));
                           
                           
                            break;
                            
                    case 2: 
                            if (trabajador == null)
                                System.out.println("Debe crear el trabajador");
                            else
                                trabajador.ver();
                            break;
                            
                    case 3: tarjeta = new Tarjeta(trabajador, 35000, null);
                            tarjeta.ver();
                            break;
                    case 4: System.out.print("Ingrese monto de la compra: ");
                            int monto = teclado.nextInt();
                            System.out.print("Ingrese su clave");
                            String clave= teclado.next();
                            if (tarjeta.validarClave(clave)) {
                                boolean acepto = tarjeta.comprar(monto);
                                if (acepto) {
                                        System.out.println("Compra realizada");
                                        tarjeta.ver();
                                } else
                                    System.out.println("Sin monto suficiente!!!");
                            } else
                                System.out.println("Clave invalida");
                            break;
                    case 5: tarjeta.ver();
                            break;
                    
                    
                }
            
        } while (opcion!=6);
        
        
        
    }
    
    
    
    public static boolean validarRut(String rut) {  
        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace(",", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
            char dv = rut.charAt(rut.length() - 1);
            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
           }
        return validacion;
    }
    
 
    
}
