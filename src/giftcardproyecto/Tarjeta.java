/*
código 6018741302285000 7149741202186111
clave 1218 1811
monto $35.000 $55.000
vigencia 30 de octubre 2020 30 de octubre 2021
trabajador trabajador
 */
package giftcardproyecto;
import java.util.Date;
import java.util.Random;
/**
 *
 * @author jpino
 */
public class Tarjeta {
    private long codigo;
    private String clave;
    private int monto;
    private Date vigencia;
    private Trabajador trabajador;
    //Constructor
    public Tarjeta(Trabajador trabajador, int monto, Date vigencia) {
        String clave;
        this.trabajador = trabajador;
        this.monto = monto;
        this.vigencia = vigencia;
        clave = trabajador.getRun().replace(".","");
        clave = clave.trim().substring(0,4);
        this.clave = clave;
        this.codigo = this.crearCodigo();
    }
    //Set - Get
    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    
    public long getCodigo() {
        return codigo;
    }

    public String getClave() {
        return clave;
    }

    public int getMonto() {
        return monto;
    }

    public Date getVigencia() {
        return vigencia;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }
    
    
    //Metodos Customer
    
    private long crearCodigo() {
        Random rnd=new Random(); //permite crear numeros aleatorios;
        long codigo = 0L;
        int digito;
        for (int i=0; i<16; i++) {
            digito = rnd.nextInt(10);
            codigo = codigo*10+digito;
        }
        return codigo;
    }
    
    private void descontar(int monto) {
        this.monto = this.monto - monto;
    }
    
    public boolean comprar(int monto) {
        if (this.monto >= monto) {
            this.descontar(monto);
            return true;
        } else
            return false;
        
            
    }
    
    public boolean validarClave(String clave) {
        return this.clave.compareTo(clave)==0;
    }
    
    
    
    public void ver() {
        this.trabajador.ver();
        System.out.println("           GIFT CARD");
        System.out.println("-------------------------------");
        System.out.println(" Codigo   : " + this.codigo);
        System.out.println(" Clave    : " + this.clave);
        System.out.println(" Vigencia : " + this.vigencia);
        System.out.println(" Monto    : $"+this.monto);        
        System.out.println("-------------------------------");
    }
    
}
